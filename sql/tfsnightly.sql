IF OBJECT_ID('tempdb.dbo.#workItemData') IS NOT NULL DROP TABLE #workItemData
IF OBJECT_ID('tempdb.dbo.#finalWorkItemData') IS NOT NULL DROP TABLE #finalWorkItemData

CREATE TABLE #workItemData (
	TFS_ID INT,
	WorkItemType VARCHAR(1000),
	CaseNumber VARCHAR(1000),
	Team VARCHAR(1000),
	SystemState VARCHAR(1000),
	StateChangeDate DATETIME,
	CreatedDate DATETIME,
	effort VARCHAR(1000)
);

INSERT INTO #workItemData
select wic.[System.Id] TFS_ID
	, wic.[System.WorkItemType] WorkItemType
	, wic.[SalesForce.CaseNumber] CaseNumber
	, wic.[VinSolutions.Team] Team
    , wic.[System.State] SystemState
	, MIN(wic.[Microsoft.VSTS.Common.StateChangeDate]) StateChangeDate
	, wic.[System.CreatedDate] CreatedDate
	, wic.[Microsoft.VSTS.Scheduling.Effort] effort
FROM [Tfs_VinSolutionsCollection].[dbo].[WorkItemChanges] wic with (nolock)
WHERE wic.[Microsoft.VSTS.Common.StateChangeDate] is not null
	and wic.[System.State] not in ( /*'done'*/, 'removed' )
GROUP BY wic.[System.Id], wic.[System.State], wic.[System.WorkItemType], wic.[SalesForce.CaseNumber],
	wic.[VinSolutions.Team], wic.[System.CreatedDate], wic.[Microsoft.VSTS.Scheduling.Effort]
UNION
select wilu.[System.Id] TFS_ID
	, wilu.[System.WorkItemType] WorkItemType
	, wilu.[SalesForce.CaseNumber] CaseNumber
	, wilu.[VinSolutions.Team] Team
    , wilu.[System.State] SystemState
	, MIN(wilu.[Microsoft.VSTS.Common.StateChangeDate]) StateChangeDate
	, wilu.[System.CreatedDate] CreatedDate
	, wilu.[Microsoft.VSTS.Scheduling.Effort] effort
FROM [Tfs_VinSolutionsCollection].[dbo].[WorkItemsLatestUsed] wilu with (nolock)
WHERE wilu.[System.WorkItemType] in ('bug','product backlog item')
	and wilu.[System.State] in ('New',
								'Approved',
								'Business Grooming', 
								'Tech Grooming',
								'In Progress', 
								'Ready for QA Deploy',
								'Ready for QA Testing',
								'QA in Progress',
								'Ready for Prod Deploy',
								'Done')
	and wilu.[Microsoft.VSTS.Common.StateChangeDate] is not null
	AND [VinSolutions.Team] IN('Skynet (Engagement)',
		'Team Guy Yeah (Vehicle)',
		'Babylon (Integration Services)',
		'Big Data Tools (Data Services)',
		'Cobra (Customer and Contact)',
		'Magnifico (Mulesoft ETL Team)',
		--'Data Services Ops',
		'Spartan (Lead and Opportunity)',
		'Tesla (Vehicles and Finance)',
		--'Analytics CP',
		'Analytics CRM',
		'Dispatch (Communication)',
		--'Opinionated Snowflakes (Architecture)',
		'Style and Script (UX)',
		'DS Vin Websites',
		'DS Vin Websites CRM',
		'DS CMS Team 3',
		'CRM Foxtrot iOS',
		'CRM Foxtrot Android',
		'CRM Foxtrot Server',
		'CRM Foxtrot Client')
GROUP BY wilu.[System.Id], wilu.[System.State], wilu.[System.WorkItemType], wilu.[SalesForce.CaseNumber],
	wilu.[VinSolutions.Team], wilu.[System.CreatedDate], wilu.[Microsoft.VSTS.Scheduling.Effort]



SELECT piv.*, wid3.WorkItemType, wid3.CaseNumber, wid3.Team, wid3.SystemState, wid3.CreatedDate, wid3.effort
INTO #finalWorkItemData
FROM 
(
	SELECT TFS_ID, StateChangeDate, SystemState
	FROM #workItemData
) wid
PIVOT
(
	MAX(StateChangeDate)
	FOR SystemState IN
		(
			--New, 
			[Business Grooming], 
			[Tech Grooming], 
			Approved, 
			[In Progress], 
			[Ready for QA Deploy], 
			[Ready for QA Testing], 
			[QA in Progress], 
			[Ready for Prod Deploy], 
			[Done]
		)
) piv
LEFT JOIN #workItemData wid3 ON
	wid3.TFS_ID = piv.TFS_ID
	AND wid3.WorkItemType IS NOT NULL
GROUP BY piv.TFS_ID, wid3.WorkItemType, wid3.CaseNumber, wid3.Team, wid3.SystemState, wid3.CreatedDate, wid3.effort,
			--New, 
			[Business Grooming], 
			[Tech Grooming], 
			Approved, 
			[In Progress], 
			[Ready for QA Deploy], 
			[Ready for QA Testing], 
			[QA in Progress], 
			[Ready for Prod Deploy], 
			[Done]


select TFS_ID
	--, isnull( convert( varchar(10), New, 101), '' ) New 
	, isnull( convert( varchar(10), CreatedDate, 101), '' ) New 
	, isnull( convert( varchar(10), [Business Grooming], 101), '' ) [Business Grooming] 
	, isnull( convert( varchar(10), [Tech Grooming], 101), '' ) [Tech Grooming]
	, isnull( convert( varchar(10), Approved, 101), '') Approved
	, isnull( convert( varchar(10), [In Progress], 101), '') [In Progress]
	, isnull( convert( varchar(10), [Ready for QA Deploy], 101), '') [Ready for QA Deploy]
	, isnull( convert( varchar(10), [Ready for QA Testing], 101), '') [Ready for QA Testing]
	, isnull( convert( varchar(10), [QA in Progress], 101), '') [QA in Progress]
	, isnull( convert( varchar(10), [Ready for Prod Deploy], 101), '') [Ready for Prod Deploy]
	, isnull( convert( varchar(10), Done, 101), '') Done
	, WorkItemType
	, case when CaseNumber is not null then 'yes' else 'no' end as [SFBug?]
	, Team 
	, isnull( effort, '') effort
from #finalWorkItemData 
	where SystemState <> 'Removed'
	--order by TFS_ID desc