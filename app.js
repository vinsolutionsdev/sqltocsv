var sql = require('mssql'),
    fs = require('fs'),
    path = require('path'),
	moment = require('moment'),
	_ = require('underscore'),
	json2csv = require('json2csv');
require('dotenv').config();

var DBConfig = {
    user: process.env.DBUSER,
    password: process.env.DBPASSWORD,
	domain: process.env.DBDOMAIN,
    server: process.env.DBSERVER,
    requestTimeout: process.env.REQUESTTIMEOUT,
    connectionTimeout: process.env.CONNECTIONTIMEOUT,
    options: {
        encrypt: false
    }
};

var DIRConfig = {
    sqlDir: process.env.SQLDIR,
    csvDir: process.env.CSVDIR,
	retention: process.env.RETENTION
};

function FindFiles(startPath, filter) {
	var fileList = []
    var files = fs.readdirSync(startPath);
    for (var i = 0; i < files.length; i++) {
        var filename = path.join(startPath, files[i]);
        if (filename.substr(filename.length - 4) == filter) {
			fileList.push({ fileName: filename });
        }
    };
	return fileList;
};

function getSQL(files) {
	for (var i = 0, len = files.length; i < len; i++) {
		files[i].Query = fs.readFileSync(files[i].fileName, 'utf8');
	}
}

function RunSQL(file) {
	var connection = new sql.Connection(DBConfig, function (err) {
		if (err) {
			console.log({ status: 'fail', data: err, code: '0001' });
			return;
		}
		if (err) {
			console.log({ status: 'fail', data: err, code: '0002' });
			return;
		}
        
        console.log( 'Making SQL request');
        
		var request = new sql.Request(connection);
		request.query(file.Query, function (err, recordset) {
			if (err) {
				err.query = file.Query;
				console.log({ status: 'fail', data: err, code: '0003' });
				return;
			}
			file.data = recordset;
			WriteResultsToFile(file);
		});
		connection.close();
	});
	connection.on('error', function (err) {
		console.log({ status: 'fail', data: err });
	});
	return;
}

function WriteResultsToFile(file) {
	var columns = [];
	var table = file.data.toTable();
	for (var i = 0, len = table.columns.length; i < len; i++) {
		columns.push(table.columns[i].name);
	}

	json2csv({ data: file.data, fields: columns }, function (err, csv) {
		if (err) {
			console.log({ status: 'fail', data: err, code: '0005' });
		}
		var savedFile = file.fileName.substr(0, file.fileName.length -4);
		var filename = savedFile + '-' + moment().format('YYYYMMDD-HHmm') + '.csv'
		fs.writeFile(filename, csv, function (err) {
			if (err) {
				console.log({ status: 'fail', data: err, code: '0004' });
			}

			console.log(filename + " was saved!");
		});
	})
}
function RemoveOldFiles(csvPath, retention) {
	var fileList = []
    var files = fs.readdirSync(csvPath);
    for (var i = 0; i < files.length; i++) {
        var filename = path.join(csvPath, files[i]);
        if (filename.substr(filename.length - 4) == '.csv') {
			 var fileStats = fs.statSync(filename);
			fileList.push({ fileName: filename, 
				            mtime: fileStats.mtime});}
    };
	var orderedList = _.sortBy(fileList, 'mtime').reverse();
	//console.log( orderedList);
	while (orderedList.length >= retention) {
		var fn = orderedList.pop()
		fs.unlinkSync(fn.fileName);
	}
	return;
}

function Main() {
	var files = FindFiles(DIRConfig.sqlDir, '.sql');
	getSQL(files);
	for (var i = 0, len = files.length; i < len; i++) {
		RunSQL(files[i]);
	}
	RemoveOldFiles(DIRConfig.csvDir, DIRConfig.retention)
	return;
}

Main();